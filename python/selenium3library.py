from selenium import webdriver

class selenium3library():
    def open_browser(self):
        self.browser = webdriver.Firefox()

    def go_to_url(self, url):
        self.browser.get(url)

    def close_browser(self):
        self.browser.quit()

    def page_should_contain_element(self, locator):
        self.browser.find_element_by_id(locator)

    def input_text(self, locator, value):
        self.browser.find_element_by_id(locator).send_keys(value)

    def get_element_value(self, locator):
        return self.browser.find_element_by_id(locator).get_attribute('value')

    def get_element_value_by_xpath(self, locator):
        return self.browser.find_elements_by_xpath(locator)[0].get_attribute('value')

    def get_element_value_by_css(self, locator):
        return self.browser.find_elements_by_css_selector(locator)[0].get_attribute('value')

    def get_elements_by_class_name(self, locator):
        return self.browser.find_elements_by_class_name(locator)

    def get_elements_by_class_name_in_id(self, id_locator, class_locator):
        return self.browser.find_element_by_id(id_locator).find_elements_by_class_name(class_locator)

    def get_elements_by_css(self, locator):
        return self.browser.find_elements_by_css_selector(locator)

    def get_element_by_css_in_id(self, id_locator, css_locator):
        return self.browser.find_element_by_id(id_locator).find_elements_by_css_selector(css_locator)

    def click_element_by_css_in_id(self, id_locator, css_locator):
        return self.browser.find_element_by_id(id_locator).find_elements_by_css_selector(css_locator).click()

    def click_element_by_css(self, locator):
        self.browser.find_elements_by_css_selector(locator)[0].click()

    def click_element_by_xpath(self, locator):
        self.browser.find_elements_by_xpath(locator)[0].click()

    def get_elements_by_xpath(self, locator):
        return self.browser.find_elements_by_xpath(locator)

    def click_element(self, locator):
        self.browser.find_element_by_id(locator).click()

    @staticmethod
    def get_web_element_innerhtml(web_element):
        return web_element.get_attribute('innerHTML')
