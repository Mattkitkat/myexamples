*** Settings ***
Library  ../python/selenium3library.py

*** Test Cases ***
My first test
    [Setup]     open browser
    Go to url   https://translate.google.com/
    Page should contain element  gt-appname
    Page should contain element  source
    Input text  source  hello
    ${ELEMENT TEXT}=  get element value  source
    Should be equal  hello  ${ELEMENT TEXT}
    Click element  gt-tl-gms
    Click element by xpath  //div[@class='goog-menuitem-content' and contains(text(), 'Maltese')]
    ${TARGET TEXT}=  get element value by css  #finish test
    Should be equal  bongu!  ${TARGET TEXT}
    [Teardown]  close browser